//
//  Article.swift
//  DynamicHeightCells
//
//  Created by Ferran Alvarez Morales on 27/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation

struct ArticleList: Decodable {
    let articles: [Article]
}

struct Article: Decodable {
    let title: String
    let description: String
}
