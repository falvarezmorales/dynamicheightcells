//
//  ArticleTableViewCell.swift
//  DynamicHeightCells
//
//  Created by Ferran Alvarez Morales on 27/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation
import UIKit

class ArticleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel! 
    
}
