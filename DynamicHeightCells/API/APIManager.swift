//
//  APIManager.swift
//  SwitzerlandWeather
//
//  Created by Ferran Alvarez Morales on 27/02/2019.
//  Copyright © 2019 Ferran Alvarez Morales. All rights reserved.
//

import Foundation

class APIManager {
    let APIEndpoint = "https://newsapi.org/v2/"
    let APIKey = "e116c4ab9aa14fad907dd021993bc0e6" //You can get a free key registering on openweathermap.org, please, use your own key.
    
    func apiRequest<Element: Decodable>(_ url: URL, _ viewModelType: Element.Type, returnRes: @escaping (Decodable) -> Void) {
        
        let weatherResource = Resource<Decodable>(url: url) { data in
            let viewModel = try? JSONDecoder().decode(viewModelType, from: data)
            return viewModel
        }
        Webservice().load(resource: weatherResource) { result in
            if let viewModel = result {
                returnRes(viewModel)
            }
        }
    }
    
}

extension APIManager {
    func getArticles(returnRes: @escaping (Decodable) -> Void) {
        
        let articlesVM = URL(string: "\(APIEndpoint)top-headlines?country=ch&apiKey=\(APIKey)")!
        apiRequest(articlesVM, ArticleList.self) { result in
            returnRes(result)
        }
    }
}
